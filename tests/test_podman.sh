#!/bin/bash -e
#
# Simple podman tests
#

# Log program and kernel versions
echo "Important package versions:"
rpm -qa | egrep 'conmon|podman|iptable|slirp|systemd' | sort | sed -e 's/^/  /'
uname -a

bats /usr/share/podman/test/system
